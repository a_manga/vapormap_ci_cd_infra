#!/bin/bash

. ../config/db-secrets

if [ -z "$DB_NAME" ] || [ -z "$MYSQL_PWD" ] || [ -z "$MYSQL_USR" ] || [ -z "$ROOT_PWD}" ]
then
    echo "Database config vars must be set !!!"
    exit 1
fi

ansible-playbook create-db-secrets.yaml --extra-vars="dbname=${DB_NAME} mysqlpwd=${MYSQL_PWD} mysqlusr=${MYSQL_USR} rootpwd=${ROOT_PWD}"

