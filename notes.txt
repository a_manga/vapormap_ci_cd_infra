==Config du proxy pour les instances
Lien: - https://openstack-community.gitlab-pages.imt-atlantique.fr/proxy_shell/
        - https://openstack-community.gitlab-pages.imt-atlantique.fr/

sudo tee -a /etc/environment << 'EOF'
export http_proxy=http://proxy.enst-bretagne.fr:8080
export https_proxy=http://proxy.enst-bretagne.fr:8080
export ftp_proxy=http://proxy.enst-bretagne.fr:8080
export no_proxy=127.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr
EOF
        

==Config du proxy pour microk8s
sudo snap set system proxy.http="http://proxy.enst-bretagne.fr:8080"
sudo snap set system proxy.https="http://proxy.enst-bretagne.fr:8080"
--> sinon timout
sudo tee -a /etc/environment << 'EOF'
HTTPS_PROXY=http://proxy.enst-bretagne.fr:8080
HTTP_PROXY=http://proxy.enst-bretagne.fr:8080
NO_PROXY=10.1.0.0/16,10.152.183.0/24,127.0.0.1
https_proxy=http://proxy.enst-bretagne.fr:8080
http_proxy=http://proxy.enst-bretagne.fr:8080
no_proxy=10.1.0.0/16,10.152.183.0/24,127.0.0.1
EOF
source /etc/environment
--> sinon: Erreur: Jan 31 16:39:26 imaster microk8s.daemon-kubelite[44166]: E0131 16:39:26.928138   44166 remote_runtime.go:116] "RunPodSandbox from runtime service failed" err="rpc error: code = Unknown desc = failed to get sandbox image \"k8s.gcr.io/pause:3.1\": failed to pull image \"k8s.gcr.io/pause:3.1\": failed to pull and unpack image \"k8s.gcr.io/pause:3.1\": failed to resolve reference \"k8s.gcr.io/pause:3.1\": failed to do request: Head \"https://k8s.gcr.io/v2/pause/manifests/3.1\": dial tcp 64.233.166.82:443: i/o timeout"
--> Cf: https://microk8s.io/docs/install-proxy
    Hint: Pour debugger:
    - ouvrir 2 terminaux sur l'instance
    - sur l'autre faire (microk8s stop)
    - sur l'un faire la commande: (tail -f /var/log/syslog) et mettre une séparation évidente (ex: ------------)
    - revenir sur le premier terminal et faire (microk8s start)
    - retourner sur le 2e term pour voir les logs et traquer l'apparition de la 1ere trace d'erreur. Dès qu'on l'a voit
    - repartir sur le 1e term et faire (microk8s stop)
    - Puis revenir récupérer tranquilement ses logs

==Install de microk8s
    https://microk8s.io/docs/getting-started
    sudo apt-get update
    sudo apt-get install snapd
    sudo snap install microk8s --classic
    sudo usermod -a -G microk8s $USER
    sudo chown -f -R $USER ~/.kube
    su - $USER
    microk8s status --wait-ready
    -->output: 
sudo microk8s enable metrics-server
sudo microk8s enable dns
sudo microk8s.enable ingress
sudo microk8s.enable storage
sudo microk8s.enable helm3

---secrets
mkdir ./.secrets
sudo tee -a .secrets/mysqldb << 'EOF'
db_vapormap
EOF
sudo tee -a .secrets/mysqlpasswd << 'EOF'
vapormap
EOF
sudo tee -a .secrets/mysqluser << 'EOF'
user_vapormap
EOF
sudo tee -a .secrets/rootpasswd << 'EOF'
vapormap
EOF

microk8s kubectl -n myapp create secret generic dbaccess \
--from-file=dbname=.secrets/mysqldb.txt \
--from-file=mysqlpwd=.secrets/mysqlpasswd.txt \
--from-file=mysqlusr=.secrets/mysqluser.txt \
--from-file=rootpwd=.secrets/rootpasswd.txt

mkdir .db-secrets && cd $_
echo -n "db_vapormap" > dbname.txt
echo -n "usr_vapormap" > user.txt
echo -n "vapormap" > rootpasswd.txt
echo -n "vapormap" > passwd.txt

echo -n "repvapormap" > reppasswd.txt

microk8s kubectl create namespace myapi

microk8s kubectl -n myapi create secret generic dbaccess \
--from-file=dbname=dbname.txt \
--from-file=mysqlusr=user.txt \
--from-file=rootpwd=rootpasswd.txt \
--from-file=mysqlpwd=passwd.txt 

--from-file=mariadb-replication-password=reppasswd.txt




===Installation du runner(Linux) sur openstack
Lien: https://gitlab.com/a_manga/vapormap-ci/-/settings/ci_cd#js-runners-settings

sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod +x /usr/local/bin/gitlab-runner
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
sudo gitlab-runner register --url https://gitlab.com/ --registration-token $REGISTRATION_TOKEN
---Interaction avec shell--
    Suite config du runner:
    Lien: https://docs.gitlab.com/runner/configuration/proxy.html
    sudo su -
    mkdir /etc/systemd/system/gitlab-runner.service.d
    touch /etc/systemd/system/gitlab-runner.service.d/http-proxy.conf
tee -a /etc/systemd/system/gitlab-runner.service.d/http-proxy.conf << 'EOF'
[Service]
Environment="HTTP_PROXY=http://proxy.enst-bretagne.fr:8080"
Environment="HTTPS_PROXY=http://proxy.enst-bretagne.fr:8080"
EOF
    exit
    sudo systemctl daemon-reload
    sudo systemctl restart gitlab-runner
    --check de la conf:
        systemctl show --property=Environment gitlab-runner
        --> output: Environment=HTTP_PROXY=http://proxy.enst-bretagne.fr:8080 HTTPS_PROXY=http://proxy.ens>
        --> vérifier visuellement que le runner est vert dans gitlab.

---Ajout de l'utilsateur "gitlab-runner" au groupe docker (existe déjà)
    sudo su -
    usermod -aG docker gitlab-runner
    exit
    sudo su - gitlab-runner
    rm -f ~/.bash_logout
    exit
    sudo usermod -a -G microk8s gitlab-runner


===helm
Lien: https://helm.sh/docs/intro/quickstart/
cd path/location/Chart.yaml
helm dependency update
--> this read the Chart.yaml depencies and downloads them into charts dir

===Install du rôle gitlab runner
ansible-galaxy install --roles-path . riemers.gitlab-runner


fatal: [kub_worker]: FAILED! => {"cache_update_time": 1645226504, "cache_updated": false, "changed": false, 
"msg": "'/usr/bin/apt-get -y -o \"Dpkg::Options::=--force-confdef\" -o \"Dpkg::Options::=--force-confold\"      
install 'snapd'' failed: E: Could not get lock /var/lib/dpkg/lock-frontend. It is held by process 4029 
(unattended-upgr)\nE: Unable to acquire the dpkg frontend lock (/var/lib/dpkg/lock-frontend), 
is another process using it?\n", "rc": 100, "stderr": "E: Could not get lock /var/lib/dpkg/lock-frontend. 
It is held by process 4029 (unattended-upgr)\nE: Unable to acquire the dpkg frontend lock (/var/lib/dpkg/lock-frontend), 
is another process using it?\n", "stderr_lines": ["E: Could not get lock /var/lib/dpkg/lock-frontend. 
It is held by process 4029 (unattended-upgr)", "E: Unable to acquire the dpkg frontend lock (/var/lib/dpkg/lock-frontend), 
is another process using it?"], "stdout": "", "stdout_lines": []}

bastion_fip = "10.129.176.174"
master_fip = "10.129.179.110"
master_priv_ip = "172.17.29.190"

microk8s.enable metrics-server
microk8s.enable dns
microk8s.enable storage
microk8s.enable helm3
microk8s.enable ingress

sudo curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64
sudo chmod +x /usr/local/bin/argocd

argocd login --core
mdp=`kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo`
argocd login --username admin --password $mdp

argocd app create vapormap --repo https://gitlab.com/a_manga/vapormap_ci_cd_infra.git --path kubernetes/manifests --dest-server https://kubernetes.default.svc --dest-namespace myapi
