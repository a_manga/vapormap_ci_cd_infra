# VaporMap


## Description

Ce projet regroupe l'infrastructure et sa configuration pour l'application Vapormap. Cette application est déployée dans un cluster Kubernetes (avec Microk8s) qui est installé dans Openstack. 



## Installation
### Prérequis
- Disposer d'un compte `OpenStack` (avec le réseau external configuré et avoir téléchargé en local le "rc file")
- Avoir une paire de clés SSH générée dans Openstack (dont on a les copies en local)
- Avoir `Terraform` d'installé en local (version >= 1.1.3 )
- Avoir `Ansible` d'installé en local (version >= 2.9.6 )
- Cloner ou télécharger ce dépôt dans une machine ayant accès au cloud `OpenStack` de l'IMT Atlantique

---
### Deux fichiers de configuration
Le fichier `config-vars` à remplir avec les données spécifiques à l'utilisateur se trouve dans le dossier `config`:
```
SSH_PATH="Absolute_path_to_my_local_ssh_directory"
SSH_KEY_FILE_PATH="Absolute_path_to_my_private_key"
PAIR_PUBKEY="Public_key_content"
```


Le deuxième fichier de configuration concerne les valeur pour la base de données.
Il se trouve dans le même répertoire `config`. C'est le fichier `db-secrets`:
```
DB_NAME="le_nom_de_la_BDD"
MYSQL_PWD="password_de_connexion_à_la_BDD"
MYSQL_USR="user_de_connexion_à_la_BDD"
ROOT_PWD="password_du_user_root"
```
Et c'est tout pour la configuration.

--- 

### Déploiement

Pour déployer l'infrastructure et l'application, voici les commandes:
- Sourcer le `rc file`: 
  -  ```source path/to/my-rc-file.sh```
- Se placer à la racine du projet
- ```make terraform```  /* dans le même shell où on a sourcé le "RC file" */
- ```make ansible``` 

Le résultat:
- une infrastructure déployée dans `OpenStack` avec `Kubernetes` (avec __Microk8s__) déjà configuré et prêt à déployer le code de l'application `vapormap` avec `Helm`;
- un `Gitlab-runner` installé et configuré dans `OpenStack` et connecté au compte Gitlab.

Attention: au cette étape l'application n'est pas encore déployée dans le cluster.

---

## Tester le déploiement
On peut déjà voir les instances déployées en se connectant à l'interface graphique `Horizon`.  
Pour que l'application soit déployée, il faut faire une petite modification:
- Activer le job `.deploy-app:` en enlevant le "." de devant.  
- Puis pousser cette modification dans Gitlab ( `git push` ).  
      
Et pour tester l'application il faut:
- Récupérer à partir d'`Horizon` l'IP flottante de l'instance nommée `kub_worker`. On peut 
- Puis mettre cette IP dans un navigateur (Port: 80) à partir d'une machine ayant accès à `OpenStack`.
- On devrait avoir la page d'accueil de l'application `vapormap`.