#!/bin/bash

. ./config-vars
if [ -z "${PAIR_PUBKEY}" ]
then
    echo "Public must be set !!!"
    exit 1
fi

export PAIR_PUBKEY="${PAIR_PUBKEY}"
envsubst '${PAIR_PUBKEY}' < tf.pubkey.template > ../terraform/pubkey.tfvars