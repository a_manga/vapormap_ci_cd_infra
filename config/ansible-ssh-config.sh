#!/bin/bash
set -e

cd ../terraform/
bastion_floating_ip=$(terraform output --raw bastion_fip)
worker_priv_ip=$(terraform output --raw kub_worker_priv_ip)
if ! expr "$bastion_floating_ip" : '[0-9][0-9]*\.[0-9]*\.[0-9]*\.[0-9]*$' >/dev/null; then
    echo "Retrieving Bastion floating and worker private IPs failed !!!"
    echo "Please check Openstack stack creation result"
    exit 1
fi

if ! expr "$worker_priv_ip" : '[0-9][0-9]*\.[0-9]*\.[0-9]*\.[0-9]*$' >/dev/null; then
    echo "Retrieving Bastion floating and worker private IPs failed !!!"
    echo "Please check Openstack stack creation result"
    exit 1
fi

cd -
linenumber=0
. ./config-vars
SSH_PATH=${SSH_PATH%/}
if [ -f "${SSH_PATH}/config" ]; then
    linenumber=$(( $(grep -n "Host kub_bastion" ${SSH_PATH}/config | cut -f1 -d:) + 1 ))   
fi

if [[ $linenumber -gt 1 ]]; then
    sed -i "${linenumber}s/Hostname [0-9,\.]*/Hostname ${bastion_floating_ip}/g" ${SSH_PATH}/config
    
    linenumber=$(( $(grep -n "Host kub_worker" ${SSH_PATH}/config | cut -f1 -d:) + 1 ))
    sed -i "${linenumber}s/Hostname [0-9,\.]*/Hostname ${worker_priv_ip}/g" ${SSH_PATH}/config
else
    tee -a ${SSH_PATH}/config <<EOF
Host kub_bastion
    Hostname ${bastion_floating_ip}
    User user
    StrictHostKeyChecking no
    PubKeyAuthentication yes
    IdentitiesOnly yes
    IdentityFile ${SSH_KEY_FILE_PATH}
    
Host kub_worker
    Hostname ${worker_priv_ip}
    User user
    IdentityFile ${SSH_KEY_FILE_PATH}
    ProxyJump kub_bastion
EOF
fi