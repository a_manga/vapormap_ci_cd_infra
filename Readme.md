# Vapormap: Mise en PROD

Dépôt Gitlab: <https://gitlab.com/a_manga/vapormap_ci_cd_infra>

QuickStart: <https://gitlab.com/a_manga/vapormap_ci_cd_infra/-/blob/master/QuickStart.md>


## Objectifs
L'objectif général est de mettre en place l'architecture et de déployer de manière automatisée l'application Vapormap. Personnellement, je me suis fixé comme objectif d'automatiser au maximum possible le déploiement de l'infrastructure dans `OpenStack` en se servant de `Terraform`, et en la configurant avec `Ansible`. Et en termes de déploiement, je voulais arriver à déployer l'application avec `Helm`.
J'ai donc opté de déployer sur `Microk8s` plutôt que directement avec `Kubernetes` pour ne pas dépenser tout le temps sur ce dernier (semble complexe à installer).

## Architecture
Avec Microk8s le choix arcitectural n'est pas compliqué: une seule VM suffit vu qu'il n'y a besoin que d'un seul node. Mais dans l'idée de pouvoir remplacer Microk8s au besoin avec Kubernetes avec plusieurs nodes, j'ai anticipé en mettant en place une instance `bastion` avec une connexion `SSH` via la commande `ProxyJump` ( configurée dans le fichier `config` du dossier `SSH` local ).  
J'ai donc installé `Microk8s` et un `Gitlab runner` dans la même instance. 

## Automatisation
C'était la partie qui m'a pris le plus de temps.
J'ai ajouté un dossier `config` à la racine du projet. Et dans ce dossier il y a 2 fichiers:  
- Un fichier pour que l'utiliseur mette ses paramètres de connexion SSH ( path du dossier SSH, clef publique, path de la clef privée ).
- Un fichier pour qu'il mette les paramètres de création et de connexion à la base de données (utilsateur, mot de passe, etc.).

Et c'est tout ce que l'utilisateur a à faire comme configuration.

### Création des instances avec Terraform
J'ai écrit un petit script `bash` ( fichier `config/tf-config.sh` ) qui source le fichier de configuration où sont saisies les informations de connexion SSH et qui génère dans le répertoire `terraform` un fichier `pubkey.tfvars`.  
Après la création des intances un objet `output` est créé. Il qui contient les adresses IP dont `Ansible` a besoin par la suite pour la configuration de ces intances créées. 

### Configuration des instances avec Ansible
Là, également j'ai écrit deux scripts `bash`:
- fichier `config/ansible-ssh-config.sh`:  
  récupérer les `output` de Terraform, récupérer les informations de connexion SSH et aller les écrire dans le fichier local `ssh/config`.
- fichier `ansible/ansible-create-db-secrets.sh`:  
récupérer la config de la base de données et passer ces paramètres au _playbook_ de la création des secrets ( de base de données ) dans `Microk8s` déjà installé.

### Utilisation de Makefile
Pour un objectif de simplicité et de lisibilité j'ai morcelé mes _playbooks_ `Ansible`. Et je les rassemble dans des targets _makefile_. Au final il n'y a besoin que de deux commandes pour tout déployer et configurer.

### Déploiement avec Helm
Une fois que je me suis assuré de l'installation du `Gitlab-runner` et de sa bonne configuration, j'ai d'abord déployé l'application manuellement (application des _manifests_ un à un). Ensuite j'ai remplacé cela ( dans le `.gitlab-ci.yaml` ) par le déploiement via `Helm`. Je me suis inspiré de l'exemple du TP qu'on a eu en cours en remplaçant notamment le chart par ma propre image _Mariadb_, et en adaptant les images _backend_ et _frontend_.

## Difficultés
L'expérience acquise lors des sessions Fil Rouge précédentes m'a permis un gain de temps sur la configuration de proxy sur `OpenStack`. Il y avait juste à automatiser les commandes de config de proxy.

### Enchaînement des commandes 
L'utilisation de makefile ne garantissait pas le bon enchaînement des commandes. Par exemple après la création d'une instance par **Terraform**, il arrive que le _Makefile_ enchâine sur la commande _Ansible de config_ alors que l'instance n'est pas encore prête ( pas accessible via SSH ).  
J'étais obligé de relancer la partie **Ansible** une deuxième voire troisième fois pour qu'**Ansible** réussisse à se connecter.  
J'ai résolu ce problème en utilisant des `task` **Ansible** de type `wait_for` qui permettent d'attendre qu'une condition soit réunie pour continuer.

### Petits problèmes bash
J'ai également eu des petits bloquages ici et là pour écrire mes scripts bash. Par exemple lors de la récupération des `output` ( des adresses IP ) de `Terraform` je m'assurais que les variables ne sont pas vides. Et je suis tombé dans un cas où je récupérais un texte d'erreur disant qu'il n'y a pas d'output: donc la vérification était OK ( alors que ce n'était des adresses IPs ). J'ai passé beaucoup de temps pour comprendre cela. Et j'ai modifié la condition par une vérification _regex_ d'IP.  

### Gestion des secrets Helm
Pour la base de données, j'avais voulu utiliser un chart `Mariadb` prêt à l'emploi fourni par `Bitnami`. Mais je n'ai pas trouvé de solution permettant de ne pas mettre en clair les détails de connexion à la base de données dans le code ( dans fichier de valeurs `values.yaml` ). D'après la documentation officielle de `Bitnami` il semble y avoir une solution. On pourrait, le cas échéant, indiquer à _Helm_, dans ce fichier de valeurs, qu'un secret a déjà été créé dans le serveur ( avec le champ `auth.existingSecret` ) et qu'il n'à qu'à utiliser ce secret pour créer la base de donner ou pour s'y connecter.  
Sauf qu'avec cette piste le serveur est bien créé (j'arrive à m'y connecter manuellement) mais le backend n'arrive pas à y créer la base de données pour une raison que j'ignore encore (à creuser).  
Plutôt que d'écrire les secrets en clair, j'ai préféré abandonné ce chart et j'utilise ma propre image `Mariadb` que je construit dans le fichier `.gitlab-ci.yml`.


## Améliorations
### Déployer avec ArgoCD
J'arrive bien à déployer manuellement avec le UI d'ArgoCD mais pas encore avec leur CLI. Je rencontre un problème de configuration ( dans le fichier `~/.kube/config` ).

### Remplacer `Microk8s` par `Kubernetes`
Pas ecore essayé.

### `LoadBalancer`
Mettre en place du LoadBalancing pour mes services. 