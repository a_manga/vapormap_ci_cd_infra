all: terraform, ansible

terraform: tf-config tf-apply
ansible: proxy-config mk8s-install gitlab-runner-install

tf-config:
	@cd ./config/; chmod u+x tf-config.sh; ./tf-config.sh

tf-apply:
	@cd ./terraform/; terraform apply -var-file="pubkey.tfvars" -auto-approve

proxy-config:
	@cd ./config/; chmod u+x ansible-ssh-config.sh; ./ansible-ssh-config.sh
	@cd ./ansible; ansible-playbook wait-for-host.yaml
	@cd ./ansible; ansible-playbook config-proxy.yaml
	
mk8s-install:
	@cd ./ansible; ansible-playbook microk8s-install.yaml
	@cd ./ansible; ansible-playbook microk8s-config.yaml
	@cd ./ansible; ansible-playbook microk8s-plugins.yaml
	@cd ./ansible; chmod u+x ansible-create-db-secrets.sh; ./ansible-create-db-secrets.sh

gitlab-runner-install:
	@cd ./ansible; chmod u+x gitlab-runner-install-cmd.sh; ./gitlab-runner-install-cmd.sh
	@cd ./ansible; ansible-playbook gitlab-runner-config.yaml

tf-init:
	@cd ./terraform/; terraform init

tf-plan:
	@cd ./terraform/; terraform plan

tf-destroy:
	@cd ./terraform/; terraform destroy -var-file="pubkey.tfvars" -auto-approve
