output "kub_worker_fip" {
  description = "IP address of floating IP for master instance"
  value       = openstack_networking_floatingip_v2.fip_master.address
}

output "kub_worker_priv_ip" {
  description = "Private IP address for master instance"
  value       = openstack_compute_instance_v2.master_instance.access_ip_v4
}

output "bastion_fip" {
  description = "IP address of floating IP for master instance"
  value       = openstack_networking_floatingip_v2.fip_bastion.address
}

