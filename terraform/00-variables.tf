# ===
# Variable définitions
# ===

variable "external_network" {
  type    = string
  default = "external"
}

variable "external_router" {
  type    = string
  default = "kubRouter"
}

# No default to force user to set the value
variable "dns_ip" {
  type = list(string)
  #default = ["208.67.222.222", "208.67.220.220"]
  default = ["192.44.75.10", "192.108.115.2"]
}

variable "user_ssh_pub_key" {}

variable "subnet_ip_range" {
  type    = string
  default = "172.17.29.0/24"
}

variable "ingress_allow_sec_port" {
  type = list(object({
    port = number
    protocol = string }
    )
  )
  default = [
      {
          port = 22
          protocol = "tcp"
      },
      {
          port = 80
          protocol = "tcp"
      },
    ]
}

variable "instance_master" {
  type    = string
  default = "kub_worker"
}

variable "instance_bastion" {
  type    = string
  default = "instance_bastion"
}

variable "master_image_name" {
  type    = string
  default = "imta-docker"
}

variable "bastion_image_name" {
  type    = string
  default = "imta-ubuntu"
}

variable "master_flavor_name" {
  type    = string
  default = "s20.xlarge"
}

variable "bastion_flavor_name" {
  type    = string
  default = "m1.small"
}

variable "key_pair_name" {
  type    = string
  default = "id_rsa"
}

variable "key_pair_pubkey" {}
