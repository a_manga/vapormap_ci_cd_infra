# ===
# Security Groups
# ===

# allow ingress ssh and http tcp protocol 
resource "openstack_compute_secgroup_v2" "ingress_allow" {
  name        = "ingress_allow"
  description = "Open input ports"

  dynamic "rule" {
    for_each = { for rule in var.ingress_allow_sec_port : rule.port => rule.protocol }
    iterator = rule
    content {
      from_port   = rule.key
      to_port     = rule.key
      ip_protocol = rule.value
      cidr        = "0.0.0.0/0"
    }
  }
}
