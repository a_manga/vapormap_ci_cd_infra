# ssh key pair to add to openstack project
resource "openstack_compute_keypair_v2" "user_key" {
  name       = var.key_pair_name
  public_key = var.key_pair_pubkey
}
