# ===
# Network
# ===

data "openstack_networking_network_v2" "ext_network" {
  name = var.external_network
}

#data "openstack_networking_router_v2" "ext_router" {
#  name = var.external_router
#}

resource "openstack_networking_router_v2" "ext_router" {
  name                = "kub_router"
  admin_state_up      = true
  external_network_id = data.openstack_networking_network_v2.ext_network.id
}

resource "openstack_networking_network_v2" "internal_net" {
  name           = "kub_net"
  admin_state_up = true
}

resource "openstack_networking_subnet_v2" "internal_sub" {
  name            = "kub_subnet"
  network_id      = openstack_networking_network_v2.internal_net.id
  cidr            = var.subnet_ip_range
  dns_nameservers = var.dns_ip
}

# Router interface configuration
resource "openstack_networking_router_interface_v2" "router_interface_1" {
  router_id = openstack_networking_router_v2.ext_router.id
  subnet_id = openstack_networking_subnet_v2.internal_sub.id
}

# Create floating ip for master and bastion instances
resource "openstack_networking_floatingip_v2" "fip_master" {
  pool = data.openstack_networking_network_v2.ext_network.name
}

resource "openstack_networking_floatingip_v2" "fip_bastion" {
  pool = data.openstack_networking_network_v2.ext_network.name
}
