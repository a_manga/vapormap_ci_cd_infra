data "template_file" "user_data_bastion" {
  template = file("./scripts/add-ssh.yaml")
  vars = {
    user_ssh_pub_key = var.key_pair_pubkey
  }
}

resource "openstack_compute_instance_v2" "std_srv_bastion" {
  name            = var.instance_bastion
  image_name      = var.bastion_image_name
  flavor_name     = var.bastion_flavor_name
  key_pair        = openstack_compute_keypair_v2.user_key.name
  security_groups = ["ingress_allow", "default"]
  user_data       = data.template_file.user_data_bastion.rendered
  network {
    name = openstack_networking_network_v2.internal_net.name
  }
}

# Attach floating ip to instance
resource "openstack_compute_floatingip_associate_v2" "bastion_http" {
  floating_ip = openstack_networking_floatingip_v2.fip_bastion.address
  instance_id = openstack_compute_instance_v2.std_srv_bastion.id
}