data "template_file" "user_data" {
  template = file("./scripts/add-ssh.yaml")
  vars = {
    user_ssh_pub_key = var.key_pair_pubkey
  }
}

resource "openstack_compute_instance_v2" "master_instance" {
  name            = var.instance_master
  image_name      = var.master_image_name
  flavor_name     = var.master_flavor_name
  key_pair        = openstack_compute_keypair_v2.user_key.name
  security_groups = ["ingress_allow", "default"]
  user_data       = data.template_file.user_data.rendered
  network {
    name = openstack_networking_network_v2.internal_net.name
  }
}

# Attach floating ip to instance
resource "openstack_compute_floatingip_associate_v2" "master_http" {
  floating_ip = openstack_networking_floatingip_v2.fip_master.address
  instance_id = openstack_compute_instance_v2.master_instance.id
}
