# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/template" {
  version = "2.2.0"
  hashes = [
    "h1:94qn780bi1qjrbC3uQtjJh3Wkfwd5+tTtJHOb7KTg9w=",
    "zh:01702196f0a0492ec07917db7aaa595843d8f171dc195f4c988d2ffca2a06386",
    "zh:09aae3da826ba3d7df69efeb25d146a1de0d03e951d35019a0f80e4f58c89b53",
    "zh:09ba83c0625b6fe0a954da6fbd0c355ac0b7f07f86c91a2a97849140fea49603",
    "zh:0e3a6c8e16f17f19010accd0844187d524580d9fdb0731f675ffcf4afba03d16",
    "zh:45f2c594b6f2f34ea663704cc72048b212fe7d16fb4cfd959365fa997228a776",
    "zh:77ea3e5a0446784d77114b5e851c970a3dde1e08fa6de38210b8385d7605d451",
    "zh:8a154388f3708e3df5a69122a23bdfaf760a523788a5081976b3d5616f7d30ae",
    "zh:992843002f2db5a11e626b3fc23dc0c87ad3729b3b3cff08e32ffb3df97edbde",
    "zh:ad906f4cebd3ec5e43d5cd6dc8f4c5c9cc3b33d2243c89c5fc18f97f7277b51d",
    "zh:c979425ddb256511137ecd093e23283234da0154b7fa8b21c2687182d9aea8b2",
  ]
}

provider "registry.terraform.io/terraform-provider-openstack/openstack" {
  version     = "1.35.0"
  constraints = "~> 1.35.0"
  hashes = [
    "h1:k1SCosvSICWAgRkswl83KtCycN7iP9asejWDDEQEtuk=",
    "zh:04cf8800c83289a28619ac9925bc03e0ccd624f0ed68284f8bd473cf48f05ef0",
    "zh:15fc2e1ea6f87d11e15aad075f3bfb7013eb63f31637f1ee317c94686c9650ec",
    "zh:1b6625ce80e6d8f192c984dcf6ba7ab303e4fdab6fa5a0a5651a8a01521aa879",
    "zh:4eb60013433f45fa3ef3fca314f68202e40ea3a12b0a5ccf75d8708002bbd8cd",
    "zh:505a7b22c813874090ceaee1a3e7f29961d0fe5a854d90f313aec5aa4900f44a",
    "zh:7be239b7e5672bd8fc3f3744bfe58dff73f8317ede349228a92dbd92b291a832",
    "zh:97b21c64da2700a69b5a3d30e514d76b52fc1089a45a0e02611aadb071b6fcc1",
    "zh:b45be0621b08f16236893a5bcc0e7fa1552176b299f6dbafa806f8b0ba5e4096",
    "zh:cd3b4387766ab33fbf504b22e73196d5984bc647f5c8ac9483ad604cf5cd2ceb",
    "zh:e3b69afe0cab18521ee11e283f783e76c02d248de40f7a25d54576f2b1643003",
    "zh:f43d52db66544065040b40db6334a1dfe6c9084d87104af1b1d133f90bbf3a66",
  ]
}
